package gorm.logical.delete

import grails.plugins.*
import org.springframework.beans.factory.config.MapFactoryBean

import com.bandithouse.grails.plugin.gormlogicaldelete.*

class GormLogicalDeleteGrailsPlugin extends Plugin {

    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "3.2.9 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
        "grails-app/views/error.gsp"
    ]

    def version = "1.0"
    def title = "Gorm Logical Delete Plugin"
    def author = "Scott Altman"
    def authorEmail = "scott@bandithouse.com"
    def description = '''\
Allows you to do a logical deletion of domain classes

This is a fork based on Sobya's Delete plugin:
https://github.com/saboya/gorm-logical-delete
'''

    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/gorm-logical-delete"

    // Extra (optional) plugin metadata

    // License: one of 'APACHE', 'GPL2', 'GPL3'
//    def license = "APACHE"

    // Details of company behind the plugin (if there is one)
//    def organization = [ name: "My Company", url: "http://www.my-company.com/" ]

    // Any additional developers beyond the author specified above.
//    def developers = [ [ name: "Joe Bloggs", email: "joe@bloggs.net" ]]

    // Location of the plugin's issue tracker.
//    def issueManagement = [ system: "JIRA", url: "http://jira.grails.org/browse/GPMYPLUGIN" ]

    // Online location of the plugin's browseable source code.
//    def scm = [ url: "http://svn.codehaus.org/grails-plugins/" ]
    def loadAfter = ['domainClass']
    Closure doWithSpring() { {->
        beans {
            logicalDeleteDomains(MapFactoryBean) { bean ->
                def classes = application.domainClasses*.clazz.findAll{
                    it.isAnnotationPresent(GormLogicalDelete)
                }
                sourceMap = classes.collectEntries { clazz ->
                    [(clazz): [
                            property    : clazz.getAnnotation(GormLogicalDelete).property(),
                            deletedState: clazz.getAnnotation(GormLogicalDelete).deletedState()
                    ]]
                }
            }
            logicalDeletePreQueryListener(PreQueryListener) { bean ->
                logicalDeleteDomains = ref('logicalDeleteDomains') // Needed for Grails < 2.3.5
            }
        }
        }
    }


    void doWithDynamicMethods() {
        // TODO Implement registering dynamic methods to classes (optional)
             DomainClassEnhancer.enhance(applicationContext.getBean('logicalDeleteDomains').keySet())
    }

    void doWithApplicationContext() {
        // TODO Implement post initialization spring config (optional)
    }

    void onChange(Map<String, Object> event) {
        // TODO Implement code that is executed when any artefact that this plugin is
        // watching is modified and reloaded. The event contains: event.source,
        // event.application, event.manager, event.ctx, and event.plugin.
    }

    void onConfigChange(Map<String, Object> event) {
        // TODO Implement code that is executed when the project configuration changes.
        // The event is the same as for 'onChange'.
    }

    void onShutdown(Map<String, Object> event) {
        // TODO Implement code that is executed when the application shuts down (optional)
    }
}
