package com.bandithouse.grails.plugin.gormlogicaldelete

import grails.compiler.ast.SupportsClassNode
import grails.compiler.traits.TraitInjector
import groovy.transform.CompileStatic
import org.codehaus.groovy.ast.ClassNode
import org.grails.compiler.injection.GrailsASTUtils

@CompileStatic
class LogicalDeleteTraitInjector implements TraitInjector, SupportsClassNode {

    @Override
    Class getTrait() {
        LogicalDeleteTrait
    }

    @Override
    String[] getArtefactTypes() {
        ['Domain'] as String[]
    }

    boolean supports(ClassNode classNode) {
        def retVal = GrailsASTUtils.hasAnnotation(classNode, GormLogicalDelete)
        return retVal
    }
}