package com.bandithouse.grails.plugin.gormlogicaldelete

import org.grails.datastore.mapping.core.Session
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.web.context.request.RequestContextHolder

trait LogicalDeleteTrait {

    private static final Logger log = LoggerFactory.getLogger(this)

    public static final String PHYSICAL_PARAM = 'logicalDelete'

    public static final String PHYSICAL_SESSION = 'physicalSession'


    void delete(){
        def gormSaveMethod = this.getMetaClass().getMetaMethod('save')
        def curriedDelete = deleteAction.curry(gormSaveMethod)
        curriedDelete(this)
    }


    public static withDeleted(Closure closure){
        def session = RequestContextHolder.currentRequestAttributes().getSession()
        session.setAttribute(PHYSICAL_SESSION, true)
        try {
            closure()
        } finally {
            session.removeAttribute(PHYSICAL_SESSION)
        }
    }


    void delete(Map m){
        def gormDeleteMethod = this.getMetaClass().getMetaMethod('delete')
        def gormDeleteWithArgsMethod = this.getMetaClass().getMetaMethod('delete', Map)
        def gormSaveMethod = this.getMetaClass().getMetaMethod('save')
        def curriedDelete = deleteAction.curry(gormSaveMethod)

        if (m.containsKey(PHYSICAL_PARAM) && !m[PHYSICAL_PARAM]) {
            if (m.size() > 1) {
                def args = m.dropWhile { it.key == PHYSICAL_PARAM }
                gormDeleteWithArgsMethod.invoke(this, args)
            } else {
                gormDeleteMethod.invoke(this)
            }
        } else {
            curriedDelete(this, m)
        }
    }


    private static deleteAction = { aSave, aDelegate, args = null ->
        log.debug "Applying logical delete to domain class ${aDelegate.class}"
        def annotation = aDelegate.getClass().getAnnotation(GormLogicalDelete)

        aDelegate[annotation.property()] = annotation.deletedState()
        if (args) aSave.invoke(aDelegate) else aSave.invoke(aDelegate, args)
    }
}

