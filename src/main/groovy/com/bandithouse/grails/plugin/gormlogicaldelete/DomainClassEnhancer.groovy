package com.bandithouse.grails.plugin.gormlogicaldelete

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.grails.datastore.mapping.core.Session

class DomainClassEnhancer {

    private static final Logger log = LoggerFactory.getLogger(this)

    public static final String PHYSICAL_PARAM = 'logicalDelete'

    public static final String PHYSICAL_SESSION = 'physicalSession'

    static void enhance(domainClasses) {
        domainClasses.each { clazz ->
     //       addListDeletedMethod(clazz)
            addTestMethod(clazz)
        }
    }

    private static void addTestMethod(clazz){
        clazz.metaClass.static.myNewMethod = {-> println "hello world" }
    }

    private static void addListDeletedMethod(clazz) {
        log.debug "Adding withDeleted method to $clazz"

        clazz.metaClass.static.withDeleted = { Closure closure ->
            delegate.withNewSession { Session session ->
                session.setSessionProperty(PHYSICAL_SESSION, true)
            }
            try {
                closure()
            } finally {
                delegate.withSession { Session session ->
                    session.clearSessionProperty(PHYSICAL_SESSION)
                }
            }
        }
    }

}
